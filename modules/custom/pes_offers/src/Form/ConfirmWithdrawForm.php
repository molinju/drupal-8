<?php

/**
 * @file
 * Contains \Drupal\pes_offers\Form\ConfirmDeleteForm.
 */

namespace Drupal\pes_offers\Form;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\pes_offers\Controller;
use Drupal\node\Entity\Node;

class ConfirmWithdrawForm extends FormBase {

  /**
   * ID of the offer.
   *
   * @var int
   */
  protected $nid;

  /**
   * ID of the application.
   *
   * @var int
   */
  protected $app;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL, $app = NULL) {
    $this->nid = $nid;
    $this->app = $app;
    $triggerElement = $form_state->getTriggeringElement();
    $node_application = Node::load($app);

    if($triggerElement['#value'] === 'Cancel' || empty($node_application)){
      $this->cancelWithdraw();
    }


    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Confirm'),
    );

    $form['cancel'] = array(
      '#type' => 'button',
      '#value' => 'Cancel',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $application_node = Node::load($this->app);
    $user_cv = $application_node->get('field_application_europass_cv')->getValue()[0]['target_id'];
    $user_letter = $application_node->get('field_application_motiv_letter')->getValue()[0]['target_id'];
    $user_attachments = $application_node->get('field_application_attachments')->getValue();

    if($user_cv){
      deleteFile($user_cv);
    }

    if($user_letter) {
      deleteFile($user_letter);
    }

    if(!empty($user_attachments)){
      foreach ($user_attachments as $item) {
        deleteFile($item['target_id']);
      }
    }

    $application_node->delete();
    drupal_set_message('Your application has been withdrawn.', 'warning');

    $response = new RedirectResponse($base_url . '/my-applications-to-offers');
    $response->send();
    $form_state->setRedirect($response);

    return;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "confirm_withdraw_form";
  }

  /**
   * {@inheritdoc}
   */
  public function cancelWithdraw() {
    global $base_url;
    $node_application = Node::load($this->app);

    if(!empty($node_application)) {
      $alias = \Drupal::service('path.alias_manager')
        ->getAliasByPath('/node/' . $this->app);
      $response = new RedirectResponse($base_url . $alias);
      $response->send();
    }
    else{
      $response = new RedirectResponse($base_url . '/my-applications-to-offers');
      $response->send();
    }
  }
}