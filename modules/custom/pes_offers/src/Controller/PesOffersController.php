<?php

namespace Drupal\pes_offers\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Access\AccessResult;


class PesOffersController extends ControllerBase {
  const PRIVATE_URI = 'private://application_files/';

  public function applyToAnOffer(NodeInterface $node) {
    global $base_url;
    $response = new RedirectResponse($base_url . '/apply/application/' . $node->id());
    $response->send();
    return;
  }

  public function withdrawApplication(NodeInterface $node) {
    global $base_url;
    $offer_id = $node->get('field_application_offer_ref')->getString();
    $offer = Node::load($offer_id);
    $nid = $offer->id();
    if (!empty($offer)) {
      // Redirect to confirmation form
      drupal_set_message('If you withdraw the application and the end date has passed, the option to apply again would not be available.', 'warning');
      $response = new RedirectResponse($base_url . "/offer/$nid/withdraw/" . $node->id());
      $response->send();
    }
  }

  public function applicationExists($nid, $uid) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'application')
      ->condition('field_application_offer_ref', $nid)
      ->condition('field_application_user_ref', $uid)
      ->execute();
    return !empty($query);
  }

  public function getApplicationByOffer($offer_nid, $uid) {
    $node = null;
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'application')
      ->condition('field_application_offer_ref', $offer_nid)
      ->condition('field_application_user_ref', $uid)
      ->execute();
    if (!empty($query)){$node = Node::load(reset($query));}
    return $node;
  }


  public function accessApplyButton(NodeInterface $node) {
    if ($node->get('type')->getString() == 'offer') {
      $nid = $node->id();
      $status = $node->get('field_pes_offer_status')->getString();
      $user = \Drupal::currentUser();
      $exists_application = $this->applicationExists($nid, $user->id());

      // If there is no application and the status is Published
      return AccessResult::allowedIf(!$exists_application && $status == 'Published');
    }
    else { // Button only showing for offer nodes
      return AccessResult::forbidden();
    }
  }

  public function accessWithdrawButton(NodeInterface $node) {
    if ($node->get('type')->getString() == 'application') {
      $offer_id = $node->get('field_application_offer_ref')->getString();
      $offer = Node::load($offer_id);
      $status = $offer->get('field_pes_offer_status')->getString();

      // If there is an application and the status is Published
      return AccessResult::allowedIf($status == 'Published' || $status == 'Ongoing');
    }
    else { // Button only showing for offer nodes
      return AccessResult::forbidden();
    }
  }
}
