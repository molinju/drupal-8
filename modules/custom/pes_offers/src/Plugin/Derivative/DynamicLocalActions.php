<?php
/**
 * @file
 * Contains \Drupal\pes_offers\Plugin\Derivative\DynamicLocalActions.
 */

namespace Drupal\pes_offers\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\pes_offers\Controller\NodeInterface;

/**
 * Defines dynamic local tasks.
 */
class DynamicLocalActions extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $current_path = explode('/', \Drupal::service('path.current')->getPath());

    $this->derivatives['pes_offers.entity.apply_to_offer'] = $base_plugin_definition;
    $this->derivatives['pes_offers.entity.apply_to_offer']['title'] = "Apply to offer";
    $this->derivatives['pes_offers.entity.apply_to_offer']['route_name'] = 'pes_offers.entity.apply_to_offer';
    $this->derivatives['pes_offers.entity.apply_to_offer']['route_parameters']['offer'] = $current_path[2];

    return $this->derivatives;
  }

}