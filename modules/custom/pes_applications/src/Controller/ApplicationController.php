<?php

namespace Drupal\pes_applications\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Entity;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Class PesProfilesAccessController.
 */
class ApplicationController extends ControllerBase {

  function getApplicationTitle($node_id) {
    $node = Node::load($node_id);
    if ($node) return $node->getTitle() . " application details";
    else return "Application details";
  }

  function getApplicationsViewTitle($node) {
    $node_offer = Node::load($node);
    if (!is_null($node_offer)) {
      $node_offer_title = $node_offer->getTitle();
      return t('Applications for ') . $node_offer_title;
    } else {
      return t('Offer applications');
    }
  }

  function isOrganizationOffer($node) {

    $user_current_int = \Drupal::currentUser();

    if ($user_current_int->hasPermission('view applications all'))
      return AccessResult::allowed();

    $user_current = User::load($user_current_int->id());
    $user_current_org = $user_current->get('field_organization')->getString();

    $node_offer = Node::load($node);
    if (!is_null($node_offer)) {
      $node_offer_author = $node_offer->getOwner();
      $node_offer_author_org = $node_offer_author->get('field_organization')->getString();
    } else {
      $node_offer_author_org = -1;
    }

    if ($user_current_org == $node_offer_author_org)
      return AccessResult::allowed();
    else
      return AccessResult::forbidden('No permission.');
  }

  function applyAccess($node_id) {

    if (\Drupal::currentUser()->hasPermission('apply offer')) {
      $offer = Node::load($node_id);
      $offer_edition = $offer->get('field_pes_offer_pes_edition')->getValue();
      $user_interface = \Drupal::currentUser();
      $user = User::load($user_interface->id());
      $user_edition = $user->get('field_applicant_pes_edition')->getString();

      if (_accessByPesEdition($offer_edition, $user_edition)) {
        return AccessResult::allowed();
      } else
        return AccessResult::forbidden('No permission.');
    }
    else
      return AccessResult::forbidden('No permission.');
  }

}
