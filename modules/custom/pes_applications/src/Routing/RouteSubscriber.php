<?php

namespace Drupal\pes_applications\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Custom access for registration pages
    if ($route = $collection->get("view.applications_of_an_offer.page_1")) {

      $defaults = $route->getDefaults();
      $defaults['_title_callback'] = '\Drupal\pes_applications\Controller\ApplicationController::getApplicationsViewTitle';
      unset($defaults['_title']);

      $requirements = $route->getRequirements();
      $requirements['_custom_access'] = '\Drupal\pes_applications\Controller\ApplicationController::isOrganizationOffer';
      unset($requirements['_roles']);
      unset($requirements['_access']);

      $route->setDefaults($defaults);
      $route->setRequirements($requirements);
    }
  }

  public static function getSubscribedEvents() {
    return parent::getSubscribedEvents();
  }
}
