<?php

namespace Drupal\pes_profiles\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Custom access for registration pages
    if ($route = $collection->get("multiple_registration.role_registration_page")) {
      $route->setRequirements(array(
        "_custom_access" => "\Drupal\pes_profiles\Controller\PesProfilesAccessController::registerAccess",
        "_method" => "GET|POST"
      ));
    }

    // Disable route to normal register page
    if ($route = $collection->get('user.register')) {
      $route->setRequirements(array(
        "_access" => "FALSE"
      ));
    }

    if ($route = $collection->get('entity.user.admin_form')){
      $route->setDefault('_form', 'Drupal\pes_profiles\Form\UserSettingsForm');
    }

    if ($route = $collection->get('view.frontpage.page_1')) {
      $route->setRequirement('_custom_access', '\Drupal\pes_profiles\Controller\PesProfilesAccessController::accessNodeList');
    }
  }

  public static function getSubscribedEvents() {
    return parent::getSubscribedEvents();
  }
}
