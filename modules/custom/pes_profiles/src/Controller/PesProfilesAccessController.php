<?php

namespace Drupal\pes_profiles\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PesProfilesAccessController.
 */
class PesProfilesAccessController extends ControllerBase {

  public function registerAccess(AccountInterface $account, $rid) {

    // Applicant form allowed for anonymous users
    if ($account->isAnonymous()) {
      if ($rid == 'applicant') return AccessResult::allowed();
      else return AccessResult::forbidden();
    }

    // Multiple_registration fix: redirects to normal user creation if role doesn't match.
    $register_forms = \Drupal::service('multiple_registration.service')->getRegistrationPages();
    $available_roles = array_keys($register_forms);
    if (!in_array($rid, $available_roles))
      throw new NotFoundHttpException();

    // TODO: Change this as a permission
    // ACAD and admins can create any account
    $roles = $account->getRoles();
    if (in_array('euipo_academy_team', $roles) || in_array('administrator', $roles)) {
      return AccessResult::allowed();
    }

    // Default access
    return AccessResult::forbidden();
  }

  public function privacyStatement() {
    $static_content = '<p><h4>Data Protection Statement on the Management of personal data in the Pan-European Seal (PES) Talent Bank</h4>
The processing of the personal data is carried out under the responsibility of the Director of the Academy acting as EUIPO data controller.<br>
This processing operation is subject to <a href="http://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1441184846677&uri=CELEX:32001R0045" target="_blank">Regulation (EC) No 45/2001 on the protection of personal data in the Community institutions and bodies</a> on the protection of individuals with regard to the processing of personal data by the Community institutions and bodies and on the free movement of such data. The information in this communication is given pursuant to Articles 11 and 12 of the Regulation (EC) N°45/2001.</p>' ;
    $static_content .= '<ol>
<li><b>What is the purpose of the processing operation?</b><br>
<p>The purpose of the PES Talent Bank is to provide a cooperation platform between the European Union Intellectual Property Office and the European Patent Office in the field of intellectual property in collaboration with other relevant European Union institutions and organisations. The PES Talent Bank provides former PES trainees a portal for submitting applications for traineeship and job opportunities.</p>
<p>The purpose of data processing operations is to allow applicants to submit their CVs and make their personal information and professional experience available to the recruitment staff of the offices posting job and traineeship opportunities.</p>
<p>In addition, during enrolment, personal data provided by applicants will be used to validate the identity of the applicant and the correctness of the information provided.</p></li>
<li><b>What personal data do we process?</b></br>
<p>First name, surname, date of birth, nationality, telephone number, address, photo, email address, year of Pan-European Seal traineeship and institution (EUIPO or EPO), PES University Member name, Education level and field of study, applications history, and all information available in users’ CVs or other attached documents.</p></li> 

<li><b>Who has access to the data and to whom is it disclosed?</b></br>
<p>The personal data is disclosed to the following recipients: The EUIPO Academy, persons in the European Patent Office responsible for the PES Programme, the DTD Department of EUIPO for management of the IT systems. and the associated partners.</p> 
<p>The information concerning any job or traineeship recruitment process will only be shared with people necessary for the implementation of such measures on a need to know basis. The data are not used for any other purposes nor disclosed to any other recipient.</p></li>

<li><b>How do we protect and safeguard your information?</b></br>
<p>All personal data related to job or traineeship recruitment procedures is stored in secure IT applications (the PES Talent Bank system and ShareDox) according to the security standards of the Office as well as in specific electronic folders accessible only to the authorised recipients.</p><p>
Appropriate security measures are applied in the management of the PES Talent Bank.</p>
<ul>
    <li>	The following standard security measures of the EUIPO Information Systems are applied:
    <ul>
<li>Information will be stored in security hardened servers with access control measures and protected by Username and Password. No anonymous access allowed.</li>
<li>Access to the website is restricted by username and password and is subject to prior validation by the EUIPO Academy.</li>
<li>Authentication and authorization to view and access information is based on roles.</li>
<li>Servers are physically protected at the Data Processing Centre.</li>
<li>Network security is configured to prevent external threats from accessing the servers.</li>
    </ul>

    </li>
    
    <li>The appropriate documents are to be signed between the EUIPO and the partner to:
    <ul>
<li>guarantee the confidentiality and the privacy of the personal data by the recipient party, e.g. ensure that it complies with, and that its acts or omissions do not cause EUIPO to be in breach of any applicable laws or regulations related to such processing including but not limited to the Data Protection Regulation (“Applicable Law”);</li> 
<li>have in place adequate contractual, technical and organisational security measures to ensure that the confidentiality of such processing is in compliance with the Applicable Law; and </li> 
<li>provide EUIPO on demand with details of the contractual, technical and organisational security measures. Separate documentation is to be drafted and send to the DPO for approval, for each of the transfers depending on the country of the recipient.</li>
    </ul>
</li> 
    <li>	The EPO and the approved associated partners are responsible for the security of information processed once they undertake a recruitment process.</li>
</ul>
</li> 


 
<li><b>How can you access, verify, modify or delete your data?</b></br>
<p>You have the right to access, rectify, block, and erase your personal data in the cases foreseen by Articles 13, 14, 15 and 16 of Regulation (EC) N°45/2001 by submitting a written request to the EUIPO data controller, Director of the Academy under <a href="mailto:Academy@euipo.europa.eu">Academy@euipo.europa.eu</a>  and by explicitly specifying your request.  The request will be processed within 3 months of its reception.</p>

<p>Please note that the data of validated accounts related to the year of Pan-European Seal traineeship is kept for statistical purposes and will not be deleted, but will not be linked to a specific candidate.</p></li> 


<li><b>What are the legal bases for processing this data?</b></br>
<p>The personal data are collected and processed in accordance with… (e.g. administrative decision or another legal instrument adopted on the basis of the Treaties):</p>
<ul>
<li>Article 5(a) of Regulation 45/2001 (“processing is necessary for the performance of a task carried out in the public interest on the basis of the Treaties establishing the European Communities or other legal instruments adopted on the basis thereof […]”)</li>
<li>Article 5(d) of Regulation 45/2001 (“the data subject has unambiguously given his or her consent”)</li> 
<li>Decision of the Executive Director of 13 July 2016 approving the PES Talent Bank</li>
</ul>
</li>

<li><b>How long do we store your data?</b></br>

<p>The time limits for storing the data are the following:</p>

<ul>

<li>Data of validated accounts, namely, accounts of former PES trainees who have completed a traineeship at the EUIPO or the EPO and who can provide as evidence a certificate for completion of such a traineeship which is to be validated by the Academy, is to be kept for as long as the account is active.</li> 

<li>Users whose accounts have not been active for a period of 2 years will be deactivated upon the receipt of an email notifying them about this.</li> 

<li>Once deactivated, personal data is maintained for three months for the purposes of restoring the account, if the user wishes to do so. After this period, personal data is removed, with the exception of the year of Pen-European Seal traineeship, which is kept for statistical purposes.</li>

<li>Accounts that have not been validated during creation or that have been considered invalid are maintained for 3 months for the purposes of allowing the user to provide evidence for the validation of the account. If no evidence is provided during this period, personal data is removed.</li>
</ul>

<p>Your personal data will be kept only for the time necessary to achieve the purpose for which they will be processed.</p>

<p>In the event of a formal appeal, all data held at the time of the formal appeal should be retained until the completion of the appeal process.</p>

</li>

<li><b>Contact Information</b></br>
<p>Should you have any queries concerning the processing of your personal data, please address them to the data controller, Director of the Academy under the following mailbox: <a href="mailto:Academy@euipo.europa.eu">Academy@euipo.europa.eu</a>.</p>
<p>You may consult EUIPO’s Data Protection Officer <a href="mailto:DataProtectionOfficer@euipo.europa.eu">DataProtectionOfficer@euipo.europa.eu</a>.</p>
<b>Appeals:</b><br> 
Complaints, in cases where the conflict is not resolved by the Data Controller and/or the Data Protection Officer, can be addressed at any time to the European Data Protection Supervisor: <a href="mailto:edps@edps.europa.eu">edps@edps.europa.eu</a>
</li> 
</ol>';
    $build = [
      '#markup' => t($static_content),
    ];
    return $build;
  }

  public function accessNodeList() {

    $user = \Drupal::currentUser();
    $roles = $user->getRoles();

    if (!in_array('administrator', $roles))
      return AccessResult::forbidden();
    else
      return AccessResult::allowed();
  }

  public  function  managementPage() {
    $html_content = '<p>This page contains a management menu which helps admin users changing some site settings. Additionaly, it contains a menu link to view site reports.</p>';
    $build = [
      '#markup' => t($html_content),
    ];
    return $build;
  }

}