<?php
/**
 * @file
 * Contains \Drupal\pes_profiles\Form\AccountTypeForm.
 */
namespace Drupal\pes_profiles\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class AccountTypeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'account_type_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $available_roles = array(
      'applicant' => t('Applicant'),
      'euipo_academy_team' => t('EUIPO Academy Team'),
      'epo' => t('EPO'),
      'associated_partner' => t('Associated Partner')
    );

    $form['account_type'] = array (
      '#type' => 'radios',
      '#title' => ('Select the type of the account you are about to create:'),
      '#options' => $available_roles,
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if(is_null($form_state->getValue('account_type'))) {
      $form_state->setErrorByName('account_type', $this->t('Please, select an account type.'));
    } else {
      $account_type = $form_state->getValue('account_type');
      $register_forms = \Drupal::service('multiple_registration.service')->getRegistrationPages();
      $register_users = array_keys($register_forms);

      if (!in_array($account_type, $register_users)) {
        $form_state->setErrorByName('account_type', $this->t('Invalid value has been introduced.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $register_forms = \Drupal::service('multiple_registration.service')->getRegistrationPages();
    $account_type = $form_state->getValue('account_type');
    $register_path = URL::fromUserInput($register_forms[$account_type]['url']);
    $form_state->setRedirectUrl($register_path);
  }

}