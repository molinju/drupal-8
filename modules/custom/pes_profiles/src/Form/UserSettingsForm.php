<?php
/**
 * Created by PhpStorm.
 * User: sancjan
 * Date: 16/08/2017
 * Time: 12:40
 */

namespace Drupal\pes_profiles\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\AccountSettingsForm;


class UserSettingsForm extends AccountSettingsForm {
  public function getFormId() {
    return parent::getFormId();
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form =  parent::buildForm($form, $form_state);
    $mail_config = parent::config('user.mail');
    $user_config = parent::config('user.settings');
    $email_token_help = $this->t('Available variables are: [site:name], [site:url], [user:display-name], [user:account-name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url].');

    //Account created
    $form['account_created'] = [
      '#type' => 'details',
      '#title' => t('Account created'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];

    $form['account_created']['account_created_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('account_created.subject'),
      '#maxlength' => 180,
    ];
    $form['account_created']['account_created_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('account_created.body'),
      '#rows' => 15,
    ];


    //Reset account
    $form['reset_account'] = [
      '#type' => 'details',
      '#title' => t('Password reset'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];

    $form['reset_account']['reset_account_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('reset_account.subject'),
      '#maxlength' => 180,
    ];
    $form['reset_account']['reset_account_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('reset_account.body'),
      '#rows' => 15,
    ];


    //Profile approved
    $form['profile_approved'] = [
      '#type' => 'details',
      '#title' => t('Profile approved'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['profile_approved']['profile_approved_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('profile_approved.subject'),
      '#maxlength' => 180,
    ];
    $form['profile_approved']['profile_approved_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('profile_approved.body'),
      '#rows' => 15,
    ];

    //Profile rejected

    $form['profile_rejected'] = [
      '#type' => 'details',
      '#title' => t('Profile rejected'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['profile_rejected']['profile_rejected_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('profile_rejected.subject'),
      '#maxlength' => 180,
    ];
    $form['profile_rejected']['profile_rejected_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('profile_rejected.body'),
      '#rows' => 15,
    ];

    //Profile changed

    $form['profile_changed'] = [
      '#type' => 'details',
      '#title' => t('Profile changed'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['profile_changed']['profile_changed_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('profile_changed.subject'),
      '#maxlength' => 180,
    ];
    $form['profile_changed']['profile_changed_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('profile_changed.body'),
      '#rows' => 15,
    ];

    //Profile reactivated
    $form['profile_reactivated'] = [
      '#type' => 'details',
      '#title' => t('Profile reactivated'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['profile_reactivated']['profile_reactivated_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('profile_reactivated.subject'),
      '#maxlength' => 180,
    ];
    $form['profile_reactivated']['profile_reactivated_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('profile_reactivated.body'),
      '#rows' => 15,
    ];

    //New offer template
    $form['new_offer'] = [
      '#type' => 'details',
      '#title' => t('New offer'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['new_offer']['new_offer_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('new_offer.subject'),
      '#maxlength' => 180,
    ];
    $form['new_offer']['new_offer_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('new_offer.body'),
      '#rows' => 15,
    ];

    //Changed offer
    $form['changed_offer'] = [
      '#type' => 'details',
      '#title' => t('Status changed offer'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['changed_offer']['changed_offer_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('changed_offer.subject'),
      '#maxlength' => 180,
    ];

    $form['changed_offer']['changed_offer_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('changed_offer.body'),
      '#rows' => 15,
    ];


    //Deactivated offer
    $form['deactivated_offer'] = [
      '#type' => 'details',
      '#title' => t('Deactivated offer'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['deactivated_offer']['deactivated_offer_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('deactivated_offer.subject'),
      '#maxlength' => 180,
    ];

    $form['deactivated_offer']['deactivated_offer_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('deactivated_offer.body'),
      '#rows' => 15,
    ];

    //News item
    $form['news_item'] = [
      '#type' => 'details',
      '#title' => t('New news item'),
      '#open' => $user_config->get('register') == USER_REGISTER_ADMINISTRATORS_ONLY,
      '#description' => t('Edit.') . ' ' . $email_token_help,
      '#group' => 'email',
    ];
    $form['news_item']['news_item_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $mail_config->get('news_item.subject'),
      '#maxlength' => 180,
    ];

    $form['news_item']['news_item_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $mail_config->get('news_item.body'),
      '#rows' => 15,
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    parent::config('user.mail')
      ->set('new_offer.body', $form_state->getValue('new_offer_body'))
      ->set('new_offer.subject', $form_state->getValue('new_offer_subject'))
      ->set('changed_offer.body', $form_state->getValue('changed_offer_body'))
      ->set('changed_offer.subject', $form_state->getValue('changed_offer_subject'))
      ->set('deactivated_offer.body', $form_state->getValue('deactivated_offer_body'))
      ->set('deactivated_offer.subject', $form_state->getValue('deactivated_offer_subject'))
      ->set('news_item.body', $form_state->getValue('news_item_body'))
      ->set('news_item.subject', $form_state->getValue('news_item_subject'))
      ->set('account_created.subject', $form_state->getValue('account_created_subject'))
      ->set('account_created.body', $form_state->getValue('account_created_body'))
      ->set('reset_account.subject', $form_state->getValue('reset_account_subject'))
      ->set('reset_account.body', $form_state->getValue('reset_account_body'))
      ->set('profile_approved.subject', $form_state->getValue('profile_approved_subject'))
      ->set('profile_approved.body', $form_state->getValue('profile_approved_body'))
      ->set('profile_rejected.body', $form_state->getValue('profile_rejected_body'))
      ->set('profile_rejected.subject', $form_state->getValue('profile_rejected_subject'))
      ->set('profile_changed.subject', $form_state->getValue('profile_changed_subject'))
      ->set('profile_changed.body', $form_state->getValue('profile_changed_body'))
      ->set('profile_reactivated.body', $form_state->getValue('profile_reactivated_body'))
      ->set('profile_reactivated.subject', $form_state->getValue('profile_reactivated_subject'))
      ->save();
  }
}