<?php

/**
 * @file
 * Contains pes_profiles.module.
 */
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements hook_install().
 */
function pes_profiles_install() {
  // Override user display config
  $module_form_cfg = \Drupal::configFactory()->getEditable('pes_profiles.user.display.form.default');
  $settings_form = \Drupal::configFactory()->getEditable('core.entity_form_display.user.user.default');
  $settings_form->setData($module_form_cfg->get())->save();

  $module_view_cfg = \Drupal::configFactory()->getEditable('pes_profiles.user.display.view.default');
  $settings_view = \Drupal::configFactory()->getEditable('core.entity_view_display.user.user.default');
  $settings_view->setData($module_view_cfg->get())->save();

  // Default organizations
  $organizations = [
    'EUIPO Academy',
    'EPO',
  ];



  foreach ($organizations as $organization) {
    $term = Term::create([
      'name' => $organization,
      'vid' => 'organization'
    ])->save();
  }

  // Default universities

  $universities = [
    'Centre for International Intellectual Property Studies (CEIPI), University of Strasbourg – France',
    'Munich Intellectual Property Law Center (MIPLC) – Germany',
    'Magister Lvcentinvs, University of Alicante – Spain',
    'Intellectual Property Law and Knowledge Management (IPKM), Maastricht University – The Netherlands',
    'Queen Mary Intellectual Property Research Institute (QMIPRI), Queen Mary University of London – UK',
    'Technical University of Vienna – Austria',
    'University of Innsbruck – Austria',
    'Centre for Intellectual Property Rights (CIR) / Interdisciplinary Centre for Law  and ICT (ICRI), Leuven – Belgium',
    'College of Europe, Bruges – Belgium',
    'Masaryk University Faculty of Law – Czech Republic',
    'Institute for Legal Protection of Intellectual Property (UPODV), Metropolitan University of Prague (MUP) – Czech Republic',
    'Tallinn University of Technology – Estonia',
    'University of Helsinki – Finland',
    'University of Turku – Finland',
    'Hanken School of Economics – Finland',
    'University Lille 2 – France',
    'University Paris 1 Panthéon-Sorbonne (Paris 1) – France',
    'University Jean Moulin Lyon 3 – France',
    'Iustinianus Primus Faculty of Law, Ss. Cyril and Methodius University (IPUKIM), Skopje – FYROM',
    'Institute of Intellectual Property, Competition and Media law (IGEWEM), Dresden University of Technology – Germany',
    'Trinity College Dublin – Ireland',
    'University College Dublin (UCD) – Ireland',
    'LUISS Guido Carli University – Italy',
    'University of Messina – Italy',
    'Riga Technical University – Latvia',
    'Jagiellonian University – Poland',
    'Łazarski University – Poland',
    'Católica Global School of Law – Portugal',
    'The Nova Law School New University of Lisbon – Portugal',
    'University of Ljubljana – Slovenia',
    'Universidad Autónoma de Madrid – Spain',
    'Universitat de València – Spain',
    'University Carlos III of Madrid – Spain',
    'Lund University – Sweden',
    'Intellectual Property Research Center, Istanbul Bilgi University (BILFIM) – Turkey',
    'University of Leeds - United Kingdom',
    'Centre for Intellectual Property Policy & Management (CIPPM), Bournemouth University – United Kingdom',
    'Centre for Intellectual Property and Information Law (CIPIL), University of Cambridge – United Kingdom',
  ];

  $weight = 0;
  foreach ($universities as $university){
    $term = Term::create([
      'name' => $university,
      'vid' => 'universities',
      'weight' => $weight,
    ])->save();
    $weight++;
  }

  //Category terms
  $categories = [
    'University',
    'Organisation / Institution',
    'Law Firm',
    'Company',
    'Consultancy'
  ];

  foreach ($categories as $category){
    $term = Term::create([
      'name' => $category,
      'vid' => 'category',
    ])->save();
  }

  //Subcategory terms
  $subcategories = [
    'General Intellectual Property law',
    'Fashion & apparel',
    'Automotive Industry',
    'Information Technology',
    'Food / Beverages',
    'Pharmaceutical field ',
    'Copyright (music, film, publishing) ',
    'EU Law and International Relations/Institutions',
    'General Law ',
    'Economics / Finance / Mathematics / Statistics',
    'Communication / Human Resources',
    'Engineering / Architecture',
    'Political Science / Sociology',
    'Linguistics / Translation',
  ];

  foreach ($subcategories as $subcategory){
    $term = Term::create([
      'name' => $subcategory,
      'vid' => 'subcategory',
    ])->save();
  }

  //General locations
  $general_locations = [
    'EU',
    'Non EU / International'
  ];

  foreach ($general_locations as $general_location){
    $term = Term::create([
      'name' => $general_location,
      'vid' => 'general_location',
    ])->save();
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function pes_profiles_menu_local_tasks_alter(&$data, $route_name) {

  // Remove tabs form register pages if not anonymous
  if ($route_name == 'multiple_registration.role_registration_page' && !\Drupal::currentUser()->isAnonymous()) {
    unset($data['tabs']);
  }

  // Normalize tab title for anonymous users
  if (isset($data['tabs'][0]['multiple_registration.local_tasks:applicant']['#link']['title'])) {
    $data['tabs'][0]['multiple_registration.local_tasks:applicant']['#link']['title'] = t('Create new account');
  }
}

/**
 * Implements hook_form_alter().
 */
function pes_profiles_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id)
{
    /* Reset password change submit function */
    if ($form_id == 'user_pass') {
        $form['#submit'][0] = '_notifyUser';
    }

    if ($form_id == 'user_register_form') {

        // Pass info to js
        $form['#attached']['drupalSettings']['formId'] = $form_id;
        if (isset($form['rid']['#value'])) $form['#attached']['drupalSettings']['role'] = $form['rid']['#value'];

        // Change account type will be not able during account creation
        if (isset($form['account']['roles'])) {
            $form['account']['roles']['#type'] = 'hidden';
            $form['account']['roles']['#disabled'] = 'disabled';
        }

        if (\Drupal::currentUser()->isAnonymous()) {
            global $base_url;
            // Privacy Link
            $privacy_link = $base_url . '/privacy_policy';

            // Disclaimer checkbox
            $form['disclaimer_desc'] = array(
                '#title' => t('I agree'),
                '#description' => t('I have read and agree to the Terms and Conditions and the <a href="' . $privacy_link . '" target="_blank">Privacy Policy</a> of the EUIPO and the PES Talent Bank, confirm that all the data I have included is correct and I authorize EUIPO to access and manage my personal data provided for the PES Talent Bank as described in the Privacy Statement.'),
                '#type' => 'checkbox',
                '#required' => true,
                '#weight' => 95
            );

            // Captcha field
            $form['captcha'] = array(
                '#type' => 'captcha',
                '#captcha_type' => 'recaptcha/reCAPTCHA',
                '#weight' => 96
            );

            //setUserStatus to active by default
            $form['actions']['submit']['#submit'][] = '_setUserStatus';

        }


        // ***********************************
        // *** Notify checkbox functionality
        // ***********************************
        if (Drupal::currentUser()->hasPermission('administer users')) {
            $form['actions']['submit']['#submit'][] = '_notifyUser';
        }

    } elseif ($form_id == 'user_form') {
        if (in_array('applicant', Drupal::currentUser()->getRoles())) {
            //Adding Europass CV template link
            $europass_cv = $form['field_applicant_europass_cv']['widget'][0]['#description'];
            if ($europass_cv) {
                $europass_cv_desc = $europass_cv->__toString() . '<div><span class="glyphicon glyphicon-search"></span>&nbsp;Download Europass CV Template&nbsp;<a href="https://europass.cedefop.europa.eu/es/documents/curriculum-vitae" target="_blank">here</a></div>';
            }
            $form['field_applicant_europass_cv']['widget'][0]['#description'] = $europass_cv_desc;
        }

        if (!Drupal::currentUser()->hasPermission('administer users')) {

            // Read only fields
            if (isset($form['field_applicant_date_of_birth'])) $form['field_applicant_date_of_birth']['#disabled'] = TRUE;
            if (isset($form['field_applicant_pes_edition'])) $form['field_applicant_pes_edition']['#disabled'] = TRUE;
            if (isset($form['field_pes_university'])) $form['field_pes_university']['#disabled'] = TRUE;
        } else {

            // Add a selector to chose the account type(role) during account edition if not applicant nor admin.
            if (isset($form['account']['roles'])) {

                // TODO: Make this list dinamic if it is posible because of bussines rules.
                $roles = array(
                    'euipo_academy_team' => t('EUIPO Academy Team'),
                    'epo' => t('EPO'),
                    'associated_partner' => t('Associated Partner')
                );

                // Get user roles.
                if ($form_state->getFormObject() instanceof EntityForm) {
                    $form_user = $form_state->getFormObject()->getEntity();
                    $user_roles = $form_user->getRoles();
                }

                // Get the real useful role.
                if (($key = array_search('authenticated', $user_roles)) !== false) {
                    unset($user_roles[$key]);
                    $user_roles_value = reset($user_roles);
                }

                // Check if is a "non-applicant" role.
                if (!in_array('applicant', $user_roles) && !in_array('administrator', $user_roles) && array_key_exists($user_roles_value, $roles)) {
                    $available_roles = $roles;
                }

                // Edit role field to show a limited list for non applicant users.
                if (!empty($available_roles)) {
                    $form['account']['roles'] = [
                        '#type' => 'select',
                        '#title' => 'Account type',
                        '#options' => $available_roles,
                        '#default_value' => $user_roles_value,
                        '#weight' => -100,
                        '#description' => t('Type of account to be created.'),
                        '#attributes' => array('class' => 'form-item-roles'),
                    ];
                } else {

                    // Disable role field in other case.
                    $form['account']['roles']['#type'] = 'hidden';
                    $form['account']['roles']['#disabled'] = TRUE;
                }
            }
        }
        //TODO JC - añadir botón de desactivar

    }

    if ($form_id == 'user_register_form' || $form_id == 'user_form') {

        // Block reason logics
        if ($form_id == 'user_register_form' && $form['rid']['#value'] != "applicant") {
            $form['field_applicant_block_reason']['widget']['#default_value'] = array();
        } else {
            $user = _getformUser($form_state);
            if (Drupal::currentUser()->hasPermission('administer users') &&
                ((!empty($form['rid']['#value']) && $form['rid']['#value'] == "applicant") || in_array('applicant', $user->getRoles()))) {
                $form['field_applicant_block_reason']['#states'] = [
                    'visible' => [
                        'input[name="status"]' => array('value' => '0'),
                    ],
                ];
                $form['#validate'][] = '_submitFormBlockReason';
            } else {
                $form['field_applicant_block_reason']['#type'] = 'hidden';
            }
        }

    }


    if ($form_id == 'user_register_form' || $form_id == 'user_form') {

        // ***********************************
        // *** Field modifications
        // ***********************************

        // *** Make email mandatory always
        if (isset($form['account']['mail'])) {
            $form['account']['mail']['#required'] = true;
        }

        // *** Remove value ALL from PES Edition
        if (isset($form['field_applicant_pes_edition'])) {
            $key = array_search('ALL', $form['field_applicant_pes_edition']['widget']['#options']);

            if ($key) {
                unset($form['field_applicant_pes_edition']['widget']['#options'][$key]);
            }
        }

        // *** Make languajes "real" mandatory for applicants
        if (isset($form['field_applicant_nationality']) && $form['field_applicant_languages']['widget']['#required'] == true) {
            unset($form['field_applicant_languages']['widget']['#options']['_none']);
        }

        // *** Remove field wrappers that dont apply.
        if (isset($form['field_applicant_attachments'])) {
            if (isset($form['rid']) && $form['rid']['#value'] != 'applicant') {
                $form['field_applicant_attachments']['#type'] = 'hidden';
            } elseif (_getRealRole($form_state) != 'applicant') {
                $form['field_applicant_attachments']['#type'] = 'hidden';
            }
        }


        // ***********************************
        // *** Organization field logic
        // ***********************************

        // *** Validate account Type
        $form['#validate'][] = '_validateUserType';

        // *** Check if not applicant
        if (isset($form['rid']['#value']) && $form['rid']['#value'] != "applicant") $org_apply = TRUE;
        elseif (_getRealRole($form_state) != "" && _getRealRole($form_state) != "applicant") $org_apply = TRUE;
        else $org_apply = FALSE;

        // *** If not applicant
        if ($org_apply) {

            // Disable Organization field when registering EPO or ACAD
            if (isset($form['rid']['#value'])) {
                if ($form['rid']['#value'] == "epo" || $form['rid']['#value'] == "euipo_academy_team") {
                    $form['field_organization']['#disabled'] = TRUE;
                    $form['field_organization']['#type'] = 'hidden';
                }
            }

            // Disable organization field if user has not permission for manage accounts.
            if (!Drupal::currentUser()->hasPermission('administer users')) {
                $form['field_organization']['#disabled'] = TRUE;
                $form['field_organization']['#type'] = 'hidden';
            } // Display organization field just when associated partner account selected
            else {
                $form['field_organization']['#states'] = array(
                    'visible' => array(
                        'select[name="roles"]' => array('value' => 'associated_partner'),
                    ),
                );
            }

            // To avoid human mistakes, delete default value of Organization when editing ACAD o EPO accounts
            // JS requires for this fields are defined in bs_pes.js
            $user_form_role = _getRealRole($form_state);
            if ($user_form_role == 'euipo_academy_team' || $user_form_role == 'epo') {
                unset($form['field_organization']['widget'][0]['target_id']['#default_value']);
            }

            // TODO: Make sure terms exist before set organization.
            $form['#validate'][] = '_validateUserOrg';
            $form['actions']['submit']['#submit'][] = '_setUserOrganization';
        }

        $form['#validate'][] = '_validateUserPassword';

        // ***********************************
        // *** When Anonymous registration
        // ***********************************

        if (Drupal::currentUser()->isAnonymous()) {
            // Not provide access to file fields.
            $form['field_applicant_europass_cv']['#access'] = false;
            $form['field_applicant_europass_cv']['widget'][0]['#required'] = false;
            $form['field_applicant_assessment_rep']['#access'] = false;
            $form['field_applicant_assessment_rep']['widget'][0]['#required'] = false;
            $form['field_applicant_mot_letter']['#access'] = false;
        } elseif (_getRealRole($form_state) == "applicant") {
            // In case applicant, check if profile has been fulfilled.
            $form['actions']['submit']['#submit'][] = '_checkIncompleteProfile';
        }


        // ***********************************
        // *** Redirection
        // ***********************************

        // *** Redirect to proper view after user creation/update
        $form['actions']['submit']['#submit'][] = '_redirectUserF<orm';


    }

    if ($form_id == 'user_form') {
        if (!Drupal::currentUser()->isAnonymous()) {
            $user = _getformUser($form_state);
            $user_id = $user->id();
            $current_user = Drupal::currentUser();
            if ($current_user->id() == $user_id) {
                $form['actions']['examples_link'] = [
                    '#title' =>'Delete my profile',
                    '#type' => 'link',
                    '#url' => Url::fromRoute('pes_profiles.delete_profile', array('id' => $user_id)),
                ];
            }
        }
        $form['actions']['submit']['#weight'] = 0;
    }
}
/**
 * Implements hook_preprocess_HOOK().
 */
function pes_profiles_preprocess_user(&$variables) {
  if ($variables['user']->isBlocked() && !Drupal::currentUser()->hasPermission('administer users')) {
    $variables['content'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="alert alert-danger" role="alert">' . t('This account has been blocked.') . '</div>',
    );
  }
}

/**
 * Implements hook_uninstall().
 */
function pes_profiles_uninstall() {
  \Drupal::configFactory()->getEditable('multiple_registration.create_registration_page_form_config')->delete();
}

/**
 * Implements hook_user_login().
 */
function pes_profiles_user_login(\Drupal\user\UserInterface $account) {
  // Default login destination to the dashboard.
  $current_request = \Drupal::service('request_stack')->getCurrentRequest();
  // Reset password login form
  $route = \Drupal::routeMatch()->getRouteName();

  if (!$current_request->query->get('destination') && $route != 'user.reset.login') {
    $current_request->query->set(
      'destination',
      Url::fromUri('internal:/')->toString()
    );
  }
}

/**
 * @param \Drupal\user\UserInterface $account
 */
function pes_profiles_user_update(\Drupal\user\UserInterface $account){
  if ($account->hasRole('applicant')) {
      //Previous user
      $original_user = $account->original;
      $profile_comp  = $account->get('field_applicant_profile_comp')->getString();
      $original_block_reason = $original_user->get('field_applicant_block_reason')->getString();
      $block_reason = $account->get('field_applicant_block_reason')->getString();
      if ($account->isActive() && $profile_comp && !$original_user->isActive() && $original_block_reason == "Under validation") {
        pes_notifications_notify_approved_profile($account);
      } else if ($account->isActive() && !$original_user->isActive() && $original_block_reason != "Under validation") {
        pes_notifications_notify_changed_profile($account);
        pes_notifications_notify_reactivated_profile($account);
      } else if (!$account->isActive() && $original_user->isActive() &&
        ($block_reason == "Inactivity" || $block_reason == "Non-appropiate content published")) {
        pes_notifications_notify_rejected_profile($account);
      } else if (!$account->isActive() && $profile_comp && $block_reason == "Under validation" && $original_user->isActive()){
        pes_notifications_notify_changed_profile($account);
      } else if (!$account->isActive() && !$original_user->isActive()){
        if ($original_block_reason == "Under validation" && $block_reason == "Non-compliant"){
          pes_notifications_notify_rejected_profile($account);
        }
      }
  }
}

/**
 * @param \Drupal\user\UserInterface $account
 */
function pes_profiles_user_insert(\Drupal\user\UserInterface $account){
  if ($account->hasRole('applicant')) {
    # Send email when a user is created as an activate one?
  }
}

/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * submitFormBlockReason when creating/editing applicant profile, check
 * the value to be set: If the user is being activated, set block reason to empty.
 * Otherwise, set block reason to proper value.
 */
function _submitFormBlockReason(&$form, FormStateInterface $form_state) {
  //Current path
  $current_path = \Drupal::service('path.current')->getPath();
  $status = $form_state->getValue('status');
  $block_reason = $form_state->getValue('field_applicant_block_reason');
  //If status is blocked and reason is empty, set error
  if ($status == 0 && ($block_reason == '' || $block_reason == array())) {
    $form_state->setErrorByName('field_applicant_block_reason', t('Block reason must be selected'));
  }
  $user = _getformUser($form_state);
  $new_value = array();
  if ($form['#id'] == 'user-form') {
    //Set block reason
    if ($status == 0) $new_value = $block_reason;
    //Getting user to set block reason with new value
    if (in_array('applicant', $user->getRoles())){
      $form_state->setValue('field_applicant_block_reason', $new_value);
    }
  } else {
    //Creating new user
    if ($status == 1) {
      $form_state->setValue('field_applicant_block_reason', $new_value);
    }
  }
}

/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * used function to check password polices (length, special characters and content)
 */
function _validateUserPassword(&$form, FormStateInterface $form_state) {
  //Check if password length is less than 8 characters
  $user_name = $form_state->getValue('name');
  $password = $form_state->getValue('pass');

  if (!empty($password)) {
    //Check if password length is lower than 8 chars.
    if (strlen($password) < 8) {
      $form_state->setErrorByName('pass', t('Make your password at least 8 characters.'));
    }

    //Detect null byte in password
    if (strpos($password, '\0') !== false) {
      $form_state->setErrorByName('pass', t('Do not use \0 character in your password.'));
    }

    //Check if password is equal to username
    if (!empty($user_name) && $user_name == $password){
      $form_state->setErrorByName('pass', t('Make your password different from your username.'));
    }

    //3 same characters or numbers or chars consecutively
    if (preg_match('/(.)\\1{2}/', $password)) {
      $form_state->setErrorByName('pass', t('Do not use subsequences of 3 repeated characters.'));
    }

    //3 same numbers consecutively
    if (strlen($password) >= 8 && hasNumericSubSequences($password)) {
      $form_state->setErrorByName('pass', t('Do not use subsequences of 3 consecutively numbers.'));
    }

    //3 same common keyboards consecutively
    if (strlen($password) >= 8 && hasKeyboardSubSequences($password)) {
      $form_state->setErrorByName('pass', t('Do not use subsequences of 3 consecutively common keyboard.'));
    }
  }
}

/**
 * @param $password, pass to be checked
 * @return bool, return if the password contains common keyboards subsequences of 3
 */

function hasKeyboardSubSequences($password) {
  $common_keyboards = array('q', 'w', 'e', 'r', 't', 'y'. 'Q', 'W', 'E', 'R', 'T', 'Y');
  $result = false;
  $length = strlen($password);
  $passwordArray = str_split($password);
  for ($i=0; $i<$length; $i++) {
    if ($i >= 2){
      if (in_array($passwordArray[$i], $common_keyboards) && in_array($passwordArray[$i-1], $common_keyboards) && in_array($passwordArray[$i-2], $common_keyboards)) {
        $result = true;
      }
    }
  }
  return $result;
}

/**
 * @param $password
 * @return bool, return if the password contains numeric subsequences of 3
 */
function hasNumericSubSequences($password) {
  $result = false;
  $length = strlen($password);
  $passwordArray = str_split($password);
  for ($i=0; $i<$length; $i++) {
    if ($i >= 2){
      if (is_numeric($passwordArray[$i]) && is_numeric($passwordArray[$i-1]) && is_numeric($passwordArray[$i-2])) {
        $result = true;
      }
    }
  }
  return $result;
}

function _validateUserType ($form, FormStateInterface $form_state) {
  if (!isset($form['rid']['#value'])) {
    $user_input = $form_state->getUserInput();
    $user_real_role = _getRealRole($form_state);

    if ($user_real_role != 'applicant' && is_null($user_input['roles'])) {
      if (isset($form['account']['roles']['#access']) && $form['account']['roles']['#access'] == false) {
        return true;
      } else {
        $form_state->setErrorByName('roles', t('Account type must be selected.'));
      }
    }
  }
}

function _validateUserOrg($form, FormStateInterface $form_state) {
  $user_input = $form_state->getUserInput();
  if ($user_input['roles'] == "associated_partner") {
    if ($user_input['field_organization'][0]['target_id'] == "") {
      $form_state->setErrorByName('field_organization', t('User organization must be selected.'));
    }
  }
}

function _setUserOrganization($form, FormStateInterface $form_state) {
  if (isset($form['rid']['#value'])) $role = $form['rid']['#value'];
  else $role = _getRealRole($form_state);

  if ($role == "epo") $organization = "EPO";
  if ($role == "euipo_academy_team") $organization = "EUIPO Academy";

  if (isset($organization)) {
    $org_term = taxonomy_term_load_multiple_by_name($organization, "organization");
    if (!empty($org_term)) $org_tid = reset($org_term)->get('tid')->value;
  }

  if (isset($org_tid)) {
    $form_user = _getformUser($form_state);
    $form_user->set('field_organization', $org_tid);
    $form_user->save();
  }
}

function _notifyUser($form, FormStateInterface $form_state) {
  if ($form['#form_id'] == "user_pass") {
    $user = $form_state->getValue('account');
    pes_notifications_notify_reset_account($user);
  } else {
    $notify = $form_state->getValue('notify');
    if ($form_state->getValue('notify')) {
      $user = _getformUser($form_state);
      pes_notifications_notify_created_account($user);
    }
  }
}

/**
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 * This method set the status of the user who is being created to active by default
 * due to Business Rules.
 */
function _setUserStatus($form, FormStateInterface $form_state) {
  $form_user = _getformUser($form_state);
  $form_user->set('status', '1');
  $form_user->save();
}


function _getformUser (FormStateInterface $form_state) {
  if ($form_state->getFormObject() instanceof EntityForm) {
    $form_user = $form_state->getFormObject()->getEntity();
  }

  if (isset($form_user)) return $form_user;
  else return NULL;
}

function _getRealRole(FormStateInterface $form_state) {
  // Get user roles.
  $form_user = _getformUser($form_state);
  if (!is_null($form_user)) $user_roles = $form_user->getRoles();

  // Get the real useful role.
  if (($key = array_search('authenticated', $user_roles)) !== false) {
    unset($user_roles[$key]);
    $user_roles_value = reset($user_roles);
  }

  if (isset($user_roles_value)) return $user_roles_value;
  else return "";
}

function _checkIncompleteProfile($form, FormStateInterface $form_state) {
  $user = _getformUser($form_state);
  $user_id = $user->id();
  $user_entity = User::load($user_id);
  $profile_comp_raw = $user_entity->get('field_applicant_profile_comp')->getValue();
  $profile_comp = reset($profile_comp_raw);

  if ($profile_comp['value'] == 0) {
    $user->set('status', '0');
    $user->set('field_applicant_block_reason', 'Under validation');
    $user->set('field_applicant_profile_comp', 1);
    $user->save();
  }
}

function _redirectUserForm($form, FormStateInterface $form_state) {
  if (!Drupal::currentUser()->isAnonymous()) {

    if (isset($form['rid']['#value'])) {
      $user_type = $form['rid']['#value'];

      if ($user_type == "applicant") $form_state->setRedirect('view.applicant_profiles.page_1');
      else $form_state->setRedirect('view.pes_users.page_1');
    }
    else {
      $user = _getformUser($form_state);
      if (!is_null($user)) {
        $form_state->setRedirect('entity.user.canonical', ['user' => $user->id()]);
      }
    }
  }
}
function _disableProfile($form, FormStateInterface $form_state) {
    if (!Drupal::currentUser()->isAnonymous()) {
        $user = _getformUser($form_state);
        $user_id = $user->id();
        $current_user = Drupal::currentUser();
        if($current_user->id() == $user_id){
            return new RedirectResponse(\Drupal\Core\Url::fromRoute('pes_profiles.delete_profile',array('id'=>$user_id)));

        }
    }
}

