<?php
namespace Drupal\pes_global_config\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Render\Markup;
use Drupal\user\Entity\User;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\KernelEvents;

class RedirectOn403Subscriber extends HttpExceptionSubscriberBase {

  protected $currentUser;

  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  protected function getHandledFormats() {
    return ['html'];
  }

  public function on403(GetResponseForExceptionEvent $event) {
    $request = $event->getRequest();
    $is_anonymous = $this->currentUser->isAnonymous();
    $route_name = $request->attributes->get('_route');
    $is_not_login = $route_name != 'user.login';
    if ($is_anonymous && $is_not_login) {
      $login_uri = Url::fromRoute('user.login')->toString();
      $returnResponse = new RedirectResponse($login_uri);
      $event->setResponse($returnResponse);
    }
  }

  public function checkAuthStatus(GetResponseEvent $event) {
    $route_name = \Drupal::routeMatch()->getRouteName();
    /**
     *  user is anonymous, only give access to edit its profile
     */
    if ($this->currentUser->isAnonymous() && $route_name == 'user.login') {
      $messages = $_SESSION['messages']['status'];
      if ($messages && sizeof($messages[0])) {
        $markup = $messages[0]->__toString();
        if (strpos($markup, 'Thank you for applying for an account') !== false) {
          $_SESSION['messages']['status'][0] = Markup::create('Your account has been created. Please, login to Pan-European Seal Talent Bank application to finish your registration process by filling out all required fields.');
        };
        if (strpos($markup, 'The changes have been saved') !== false) {
          unset($_SESSION['messages']['status']);
        }
      }
    }

    if ($this->currentUser->isAnonymous() && $route_name != 'user.login') {

      // add logic to check other routes you want available to anonymous users,
      // otherwise, redirect to login page.
      $route_name = \Drupal::routeMatch()->getRouteName();
      if ($route_name == 'multiple_registration.role_registration_page' || $route_name == 'user.pass' || $route_name == 'user.reset' || $route_name == 'user.reset.form' || $route_name == 'user.reset.login'
      || $route_name == 'pes_profiles.privacy_policy') {
        return;
      }

      $login_uri = Url::fromRoute('user.login')->toString();
      $returnResponse = new RedirectResponse($login_uri);
      $event->setResponse($returnResponse);
    }

    if (in_array('applicant', $this->currentUser->getRoles())) {
      //Check if the applicant profile is complete or not
      if ($route_name == 'entity.user.edit_form' || $route_name == 'user.logout') {
        return;
      }

      //If you are an applicant, but your profile has not been completed,
      // then the app redirects you to edit profile page until your profile is completed.
      $account = User::load($this->currentUser->id());
      $profile_compl = $account->get('field_applicant_profile_comp')->getString();
      if (!$profile_compl) {
        $login_uri = Url::fromRoute('entity.user.edit_form', ['user' => $this->currentUser->id()])->toString();
        $returnResponse = new RedirectResponse($login_uri);
        $event->setResponse($returnResponse);
      }
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = ['onException', static::getPriority()];
    $events[KernelEvents::REQUEST][] = array('checkAuthStatus');
    return $events;
  }

}