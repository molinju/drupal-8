<?php

/* themes/custom/bs_pes/templates/views/views-view.html.twig */
class __TwigTemplate_98f15ed0e82ad8dc806092a2b28620e12ff41ea856f91450de6962501d6a2a93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 36, "if" => 46);
        $filters = array("clean_class" => 38);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if'),
                array('clean_class'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 36
        $context["classes"] = array(0 => "view", 1 => ("view-" . \Drupal\Component\Utility\Html::getClass(        // line 38
($context["id"] ?? null))), 2 => ("view-id-" .         // line 39
($context["id"] ?? null)), 3 => ("view-display-id-" .         // line 40
($context["display_id"] ?? null)), 4 => ((        // line 41
($context["dom_id"] ?? null)) ? (("js-view-dom-id-" . ($context["dom_id"] ?? null))) : ("")));
        // line 44
        echo "<div";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ($context["classes"] ?? null)), "method"), "html", null, true));
        echo ">
  ";
        // line 45
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_prefix"] ?? null), "html", null, true));
        echo "
  ";
        // line 46
        if (($context["title"] ?? null)) {
            // line 47
            echo "    ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
            echo "
  ";
        }
        // line 49
        echo "  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title_suffix"] ?? null), "html", null, true));
        echo "

  <div class=\"row\">

    <div ";
        // line 53
        if (($context["exposed"] ?? null)) {
            echo " class=\"col-md-3\" ";
        }
        echo ">

        ";
        // line 55
        if ((($context["header"] ?? null) && ($context["exposed"] ?? null))) {
            // line 56
            echo "            <div class=\"view-header\">
                ";
            // line 57
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["header"] ?? null), "html", null, true));
            echo "
            </div>
        ";
        }
        // line 60
        echo "
        ";
        // line 61
        if (($context["exposed"] ?? null)) {
            // line 62
            echo "        <div class=\"view-filters form-group\">
          ";
            // line 63
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["exposed"] ?? null), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 66
        echo "      ";
        if (($context["attachment_before"] ?? null)) {
            // line 67
            echo "        <div class=\"attachment attachment-before\">
          ";
            // line 68
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["attachment_before"] ?? null), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 71
        echo "
    </div>

    <div ";
        // line 74
        if (($context["exposed"] ?? null)) {
            echo " class=\"col-md-9\" ";
        } else {
            echo " class=\"col-md-12\" ";
        }
        echo ">

        ";
        // line 76
        if ((($context["header"] ?? null) &&  !($context["exposed"] ?? null))) {
            // line 77
            echo "            <div class=\"view-header\">
                ";
            // line 78
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["header"] ?? null), "html", null, true));
            echo "
            </div>
        ";
        }
        // line 81
        echo "

        ";
        // line 83
        if (($context["rows"] ?? null)) {
            // line 84
            echo "        <div class=\"view-content\">
          ";
            // line 85
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["rows"] ?? null), "html", null, true));
            echo "
        </div>
      ";
        } elseif (        // line 87
($context["empty"] ?? null)) {
            // line 88
            echo "        <div class=\"view-empty\">
          ";
            // line 89
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["empty"] ?? null), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 92
        echo "
      ";
        // line 93
        if (($context["pager"] ?? null)) {
            // line 94
            echo "        ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["pager"] ?? null), "html", null, true));
            echo "
      ";
        }
        // line 96
        echo "      ";
        if (($context["attachment_after"] ?? null)) {
            // line 97
            echo "        <div class=\"attachment attachment-after\">
          ";
            // line 98
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["attachment_after"] ?? null), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 101
        echo "      ";
        if (($context["more"] ?? null)) {
            // line 102
            echo "        ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["more"] ?? null), "html", null, true));
            echo "
      ";
        }
        // line 104
        echo "
    </div>

  </div>

  ";
        // line 109
        if (($context["footer"] ?? null)) {
            // line 110
            echo "    <div class=\"view-footer\">
      ";
            // line 111
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["footer"] ?? null), "html", null, true));
            echo "
    </div>
  ";
        }
        // line 114
        echo "  ";
        if (($context["feed_icons"] ?? null)) {
            // line 115
            echo "    <div class=\"feed-icons\">
      ";
            // line 116
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["feed_icons"] ?? null), "html", null, true));
            echo "
    </div>
  ";
        }
        // line 119
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/bs_pes/templates/views/views-view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 119,  224 => 116,  221 => 115,  218 => 114,  212 => 111,  209 => 110,  207 => 109,  200 => 104,  194 => 102,  191 => 101,  185 => 98,  182 => 97,  179 => 96,  173 => 94,  171 => 93,  168 => 92,  162 => 89,  159 => 88,  157 => 87,  152 => 85,  149 => 84,  147 => 83,  143 => 81,  137 => 78,  134 => 77,  132 => 76,  123 => 74,  118 => 71,  112 => 68,  109 => 67,  106 => 66,  100 => 63,  97 => 62,  95 => 61,  92 => 60,  86 => 57,  83 => 56,  81 => 55,  74 => 53,  66 => 49,  60 => 47,  58 => 46,  54 => 45,  49 => 44,  47 => 41,  46 => 40,  45 => 39,  44 => 38,  43 => 36,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/bs_pes/templates/views/views-view.html.twig", "C:\\xampp\\htdocs\\paneuropean-seal\\themes\\custom\\bs_pes\\templates\\views\\views-view.html.twig");
    }
}
